#include "CombinedSink.h"

#include "DataSinkRegistry.h"
REGISTER_DATASINK(CombinedSink)

CombinedSink::CombinedSink(const std::string& name)
  : IDataSink(name)
{ }

void CombinedSink::setConfiguration(const nlohmann::json& config)
{ }

void CombinedSink::addSink(std::shared_ptr<IDataSink> sink)
{ m_sinks.push_back(sink); }

void CombinedSink::init()
{ }

void CombinedSink::setTag(const std::string& name, const std::string& value)
{
  std::for_each(m_sinks.begin(), m_sinks.end(),
		[&name, &value](std::shared_ptr<IDataSink> sink){ sink->setTag(name, value); });
}

void CombinedSink::setTag(const std::string& name, int8_t value)
{
  std::for_each(m_sinks.begin(), m_sinks.end(),
		[&name, &value](std::shared_ptr<IDataSink> sink){ sink->setTag(name, value); });
}

void CombinedSink::setTag(const std::string& name, int32_t value)
{
  std::for_each(m_sinks.begin(), m_sinks.end(),
		[&name, &value](std::shared_ptr<IDataSink> sink){ sink->setTag(name, value); });
}

void CombinedSink::setTag(const std::string& name, int64_t value)
{
  std::for_each(m_sinks.begin(), m_sinks.end(),
		[&name, &value](std::shared_ptr<IDataSink> sink){ sink->setTag(name, value); });
}

void CombinedSink::setTag(const std::string& name, double value)
{
  std::for_each(m_sinks.begin(), m_sinks.end(),
		[&name, &value](std::shared_ptr<IDataSink> sink){ sink->setTag(name, value); });
}

void CombinedSink::startMeasurement(const std::string& measurement, std::chrono::time_point<std::chrono::system_clock> time)
{
  std::for_each(m_sinks.begin(), m_sinks.end(),
		[&measurement, &time](std::shared_ptr<IDataSink> sink){ sink->startMeasurement(measurement, time); });
}

void CombinedSink::setField(const std::string& name, const std::string& value)
{
  std::for_each(m_sinks.begin(), m_sinks.end(),
		[&name, &value](std::shared_ptr<IDataSink> sink){ sink->setField(name, value); });
}

void CombinedSink::setField(const std::string& name, int8_t value)
{
  std::for_each(m_sinks.begin(), m_sinks.end(),
		[&name, &value](std::shared_ptr<IDataSink> sink){ sink->setField(name, value); });
}

void CombinedSink::setField(const std::string& name, int32_t value)
{
  std::for_each(m_sinks.begin(), m_sinks.end(),
		[&name, &value](std::shared_ptr<IDataSink> sink){ sink->setField(name, value); });
}

void CombinedSink::setField(const std::string& name, int64_t value)
{
  std::for_each(m_sinks.begin(), m_sinks.end(),
		[&name, &value](std::shared_ptr<IDataSink> sink){ sink->setField(name, value); });
}

void CombinedSink::setField(const std::string& name, double value)
{
  std::for_each(m_sinks.begin(), m_sinks.end(),
		[&name, &value](std::shared_ptr<IDataSink> sink){ sink->setField(name, value); });
}

void CombinedSink::recordPoint()
{
  std::for_each(m_sinks.begin(), m_sinks.end(),
		[](std::shared_ptr<IDataSink> sink){ sink->recordPoint(); });
}

void CombinedSink::endMeasurement()
{
  std::for_each(m_sinks.begin(), m_sinks.end(),
		[](std::shared_ptr<IDataSink> sink){ sink->endMeasurement(); });
}
