#ifndef CALIBRATEDDEVICE_H
#define CALIBRATEDDEVICE_H

#include <cstdint>
#include <vector>
#include <memory>
#include <map>

#include "DeviceCalibration.h"
#include "DummyCalibration.h"

//! \brief Abstract implementation of a calibrated device.
/**
 * A calibrated device is a device that converts between digital units and
 * a physical quantity. For example, an ADC that converts an input analogue
 * voltage to a digital quantity.
 *
 * This class provides a common base for such devices to handle storing of
 * calibrations (via `DeviceCalibration`) and the application.
 *
 * ## Reading/Writing
 *
 * Controlling a calibrated device is reading or writing (or both) digital values,
 * indepdenent of device type. The base `CalibratedDevice` class implements the
 * `read`/`write` functions that deal with the calibration and call the digital
 * `readCount`/`writeCount` functions.
 *
 * The default implementation of the digital read/write functions throws
 * a `NotSupportedException` exception. The supported operations need to be 
 * implemented in the device subclass.
 *
 * The write function returns the real value set. This is needed as not all
 * analogue values can be represented using the discrete representations of
 * a device.
 *
 * ## Multi-Channel Devices
 * For multi-channel ADC's, a per-channel calibration can be applied using the 
 * `setCalibration(ch,...)` function. If no per-channel calibration is set for
 * a specific channel, then the global calibration (set via constructor or
 * `setCalibration(...)`) is used.
 *
 * The calibration of a channel should be retrieved via the `findCalibration`
 * function. This handles the case of per-channel settings as described above.
 */
class CalibratedDevice
{
public:
  /**
   * \param calibration Global calibration for all channels.
   */
  CalibratedDevice(std::shared_ptr<DeviceCalibration> calibration=std::make_shared<DummyCalibration>());
  virtual ~CalibratedDevice() =default;

  //! Set global calibration
  /**
   * \param calibration Global calibration for all channels.
   */  
  void setCalibration(std::shared_ptr<DeviceCalibration> calibration);

  //! \brief Set per-channel calibration
  /**
   * Override global calibration for specific channels.
   *
   * \param ch Channel to calibrate
   * \param calibration Calibration for `ch`
   */ 
  void setCalibration(uint8_t ch, std::shared_ptr<DeviceCalibration> calibration);

  //! Read calibrated value of channel `ch`
  /**
   * \param ch Channel to read
   *
   * \return result of `readCount`, followed by calibration
   */  
  double read(uint8_t ch=0);

  //! Bulk read calibrated values of channels `chs`
  /**
   * The `data` vector used to store the results does not 
   * need to be the same size as `chs`. It will be cleared
   * and allocated to the right size by this function.
   *
   * \param chs List of channel numbers to read
   * \param data Vector where readings will be stored
   *
   * \return result of `readCount`, followed by calibration.
   */
  void read(const std::vector<uint8_t>& chs, std::vector<double>& data);

  //! Read value of channel `ch` (counts)
  /**
   * Implementation of reading from the device goes here
   * 
   * \param ch Channel to read
   */
  virtual int32_t readCount(uint8_t ch=0);

  //! Bulk read values of channels `chs` (counts)
  /**
   * Implementation of reading from the ADC goes here
   * 
   * The `data` vector used to store the results does not 
   * need to be the same size as `chs`. The implementatino
   * of `readCount` should clear and allocated the vector
   * to the right size.
   *
   * \param chs List of channel numbers to read
   * \param data Vector where readings will be stored
   */  
  virtual void readCount(const std::vector<uint8_t>& chs, std::vector<int32_t>& data);

  //! Write calibrated value of channel `ch`
  /**
   * \param value Value to write
   * \param ch Channel to write
   *
   * \return Actual value written
   */  
  double write(double value, uint8_t ch=0);

  //! Bulk write calibrated values of channels `chs`
  /**
   * The `actvalues` vector used to store the results does not 
   * need to be the same size as `chs`. It will be cleared
   * and allocated to the right size by this function.
   *
   * \param chs List of channel numbers to write
   * \param values Vector with target values
   * \param actvalues Vector where actual values will be stored
   */
  void write(const std::vector<uint8_t>& chs, const std::vector<double>& values, std::vector<double>& actvalues);

  //! Write value of channel `ch` (counts)
  /**
   * Implementation of writeing from the device goes here
   * 
   * \param value Value to write to
   * \param ch Channel to write
   */
  virtual void writeCount(int32_t value, uint8_t ch=0);

  //! Bulk write values of channels `chs` (counts)
  /**
   * Implementation of writing to the device goes here
   * 
   * \param chs List of channel numbers to write
   * \param values Vector where writeings will be stored
   */
  virtual void writeCount(const std::vector<uint8_t>& chs, const std::vector<int32_t>& values);

protected:
  //! \brief Find `DeviceCalibration` for given channel
  /**
   * The global calibration is returned if `ch` does not have a per-channel calibration
   * set.
   *
   * \return The calibration to be used for the given channel. (per-channel or global)
   */
  std::shared_ptr<DeviceCalibration> findCalibration(uint8_t ch=0) const;

private:
  std::shared_ptr<DeviceCalibration> m_calibration;
  std::map<uint8_t, std::shared_ptr<DeviceCalibration>> m_channelCalibration;
};

#endif // CALIBRATEDDEVICE_H
