#ifndef FILECALIBRATION_H
#define FILECALIBRATION_H

#include "DeviceCalibration.h"

#include <string>
#include <vector>

//! \brief Calibration read from a file containing values at specific points.
/**
 * Calibration read from a file containing values at specific points. Linear
 * interpolation is used to get the calibration between points.
 *
 * Syntax of the file is as follows:
 * ```
 * counts value
 * c0 v0
 * c1 v1
 * ...
 * cn vn
 * ```
 *
 * The first line is a header. The remaining are measurment points with
 * the first column being the counts and the second the calibrated value.
 */
class FileCalibration : public DeviceCalibration
{
public:
  /**
   * \param path Path to the file containing the calibration table
   */
  FileCalibration(const std::string& path);
  virtual ~FileCalibration();

  virtual double calibrate(int32_t counts);
  virtual int32_t uncalibrate(double value);

private:
  std::vector<double> m_counts;
  std::vector<double> m_values;
};

#endif // FILECALIBRATION_H
