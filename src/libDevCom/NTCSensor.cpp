#include "NTCSensor.h"
#include "DeviceComRegistry.h"
REGISTER_DEVCOM(NTCSensor, ClimateSensor)

#include "NotSupportedException.h"

#include <cmath>

NTCSensor::NTCSensor(uint8_t chan, std::shared_ptr<ADCDevice> dev, bool steinhart, float Tref_A, float Rref_B, float Bntc_C,bool readoverntc, float Rdiv, float Vsup)
  : m_adcdev(dev), m_chan(chan), m_Tref(Tref_A), m_Rref(Rref_B), m_Bntc(Bntc_C), m_shA(Tref_A), m_shB(Rref_B), m_shC(Bntc_C), m_Rdiv(Rdiv), m_Vsup(Vsup), m_readoverntc(readoverntc), m_steinhart(steinhart)
{ }

NTCSensor::~NTCSensor()
{ }

void NTCSensor::init()
{ }

void NTCSensor::reset()
{ }

void NTCSensor::read()
{
  m_temperature = RtoC(getRntc(m_adcdev->read(m_chan)));
}

float NTCSensor::temperature() const
{
  return m_temperature;
}

float NTCSensor::humidity() const
{
  throw NotSupportedException("Humidity not supported for NTC");
  return 0;
}

float NTCSensor::pressure() const
{
  throw NotSupportedException("Pressure not supported for NTC");
  return 0;
}

float NTCSensor::getRntc(float Vout)
{
  // If there is a reading error, e.g. open circuit, return a default value
  float Rntc = std::numeric_limits<float>::max(); //3.40282e+38
  if (!m_readoverntc)
  {
    if(Vout<=0.01*m_Vsup) return std::numeric_limits<float>::max();
    Rntc = m_Rdiv*(m_Vsup/Vout-1.);
  } else {
    if(Vout>=0.99*m_Vsup) return std::numeric_limits<float>::max();
    Rntc = m_Rdiv*Vout/(m_Vsup-Vout);
  }
  return Rntc;
}

float NTCSensor::RtoC(float Rntc)
{
  if (Rntc >= 3.e+38) return -273.15;
  if (m_steinhart)
  {
    float logres = log(Rntc);
    float absT = 1./(m_shA+m_shB*logres+m_shC*pow(logres,3));
    return absT-273.15;
  }
  return m_Bntc*m_Tref/(m_Tref*std::log(Rntc/m_Rref)+m_Bntc) - 273.15;
}
