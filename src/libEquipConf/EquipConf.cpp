#include "EquipConf.h"

#include <iostream>
#include <fstream>
#include <algorithm>
#include <string>

#include "PowerSupplyRegistry.h"
#include "ComRegistry.h"
#include "Logger.h"

#include "Logger.h"
using json = nlohmann::json;

using json = nlohmann::json;

////////////////////
// Configuration
////////////////////

EquipConf::EquipConf()
{ }

EquipConf::~EquipConf()
{ }

EquipConf::EquipConf(const std::string& hardwareConfigFile)
{
  setHardwareConfig(hardwareConfigFile);
}

EquipConf::EquipConf(const json& hardwareConfig)
{
  setHardwareConfig(hardwareConfig);
}

void EquipConf::setHardwareConfig(const std::string& hardwareConfigFile)
{
  //load JSON object from file
  std::ifstream i(hardwareConfigFile);
  if(!i.good()) {
    throw std::runtime_error("Provided equipment configuration file \"" + hardwareConfigFile + "\" could not be found or opened");
  }
  i >> m_hardwareConfig;
}

void EquipConf::setHardwareConfig(const json& hardwareConfig)
{
  //store JSON config file
  m_hardwareConfig = hardwareConfig;
}

json EquipConf::getDeviceConf(const std::string& label)
{
  for (const auto& hw : m_hardwareConfig["devices"].items()) {
    //check label 
    if (hw.key() == label) return hw.value();
  }
  return json();
}

json EquipConf::getChannelConf(const std::string& label)
{
  for (const auto& ch : m_hardwareConfig["channels"].items()) {
    //check label 
    if (ch.key() == label) return ch.value();
  }
  return json();
}

////////////////////
// General private
////////////////////


////////////////////
// Power-Supply
////////////////////

std::shared_ptr<IPowerSupply> EquipConf::getPowerSupply(const std::string& name)
{
  //first check if an object with the same name/type is already available (was already instantiated)
  if (m_listPowerSupply.find(name) != m_listPowerSupply.end()) {
    return m_listPowerSupply[name];
  }

  //Otherwise, create the object
  //check first if hardware configuration is available
  json reqPSConf = getDeviceConf(name);
  if (reqPSConf.empty()) {
    logger(logWARNING) << "Requested device not found in input configuration file: " << name;
    return nullptr;
  }
  if (reqPSConf["hw-type"] != "PS") {
    logger(logWARNING) << "Requested power supply device has wrong hw-type field in configuration." << name << ", type = " << reqPSConf["hw-type"];
    return nullptr;    
  }

  //setup device
  std::shared_ptr<IPowerSupply> ps = EquipRegistry::createPowerSupply(reqPSConf["hw-model"], name);

  // configure already the PS
  ps->setConfiguration(reqPSConf);

  // Setup communication object
  if(!reqPSConf.contains("communication"))
    {
      logger(logWARNING) << "Requested device is missing communication configuration";
      return nullptr;
    }
  ps->setCom(createCommunication(reqPSConf["communication"]));

  // Check
  ps->checkCompatibilityList();

  return ps;
}

std::shared_ptr<PowerSupplyChannel> EquipConf::getPowerSupplyChannel(const std::string& name)
{
  //first check if an object with the same name/type is already available (was already instantiated)
  if (m_listPowerSupplyChannel.find(name) != m_listPowerSupplyChannel.end())
    return m_listPowerSupplyChannel[name];

  //Otherwise, create the object
  //check first if hardware configuration is available
  json reqChConf = getChannelConf(name);
  if (reqChConf.empty())
    {
      logger(logWARNING) << "Requested channel not found in input configuration file: " << name;
      return nullptr;
    }

  if (reqChConf["hw-type"] != "PS")
    {
      logger(logWARNING) << "Requested power supply channel has wrong hw-type field in configuration." << name << ", type = " << reqChConf["hw-type"];
      return nullptr;
    }

  // Get physical power supply
  std::shared_ptr<IPowerSupply> ps = getPowerSupply(reqChConf["device"]);
  if(ps==nullptr)
    {
      logger(logWARNING) << "Cannot fetch requested power supply: " <<  reqChConf["device"];
      return nullptr;
    }    

  // Other settings
  unsigned ch = reqChConf["channel"];

  // Create!
  std::shared_ptr<PowerSupplyChannel> theChannel = std::make_shared<PowerSupplyChannel>(name, ps, ch);

  //configure already the PS
  if(reqChConf.contains("program"))
    theChannel->setProgram(reqChConf["program"]);

  return theChannel;
}

std::shared_ptr<ICom> EquipConf::createCommunication(const json& config) const
{
  if (!config.contains("protocol"))
    throw std::runtime_error("Communicaiton block is missing protocol definition.");

  std::shared_ptr<ICom> com=EquipRegistry::createCom(config["protocol"]);
  com->setConfiguration(config);
  com->init();

  return com;
}
