#include "AgilentE36300APs.h"

//Register power supply
#include "PowerSupplyRegistry.h"
REGISTER_POWERSUPPLY(AgilentE36300APs)

AgilentE36300APs::AgilentE36300APs(const std::string& name) :
AgilentPs(name, {"E36311A", "E36312A", "E36313A"}, 3)
{ }
