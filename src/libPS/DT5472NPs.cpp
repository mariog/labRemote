#include "DT5472NPs.h"

#include "TextSerialCom.h"

#include <algorithm>
#include <thread>

#include "Logger.h"

//Register power supply
#include "PowerSupplyRegistry.h"
REGISTER_POWERSUPPLY(DT5472NPs)

DT5472NPs::DT5472NPs(const std::string& name)
: DT54xxPs(name, {"DT5472"}, Polarity::Negative, 105e-6)
{ }
